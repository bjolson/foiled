function [x,y] = circle(npts,xc,yc,alpha,Lx,Ly)

% Make points describing a square

% 1) Make unit circle
thetaN = linspace(-pi,pi,npts);
theta = thetaN(1:end-1);
r = 1.0;
x = r * cos( theta );
y = r * sin( theta );

% 2) Rotate and scale
x = x*Lx;
y = y*Ly;

dd = [cos(alpha), sin(alpha); -sin(alpha), cos(alpha)] * [x ; y];
x = dd(1,:);
y = dd(2,:);

% 3) Add offset
x = x + xc;
y = y + yc;


end
