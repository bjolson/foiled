import numpy as npy
# ------------------------ START OF INPUT PARAMETER REGION ------------------- %

# Foil geometry
c = 1;                #% Geometric chord length
s = 2;                #% Span (along y-axis)
alpha = 0.087266;     #% Angle of attack (in radians)
NACA = [0, 0, 1, 5];     #% NACA 4-digit designation as a row vector;

# Surface resolution parameters
Ni = 100;            #% Number of interpolation points along the foil

# ------------------------- END OF INPUT PARAMETER REGION -------------------- %

# Create a vector with x-coordinates, camber and thickness
beta = npy.linspace(0,npy.pi,Ni);
x = c*(0.5*(1-npy.cos(beta)));
z_c = npy.zeros(x.shape);
z_t = npy.zeros(x.shape);
theta = npy.zeros(x.shape);


# Values of m, p and t
m = NACA[0]/100;
p = NACA[1]/10;
t = (NACA[2]*10 + NACA[3])/100;


# Calculate thickness
# The upper expression will give the airfoil a finite thickness at the trailing
# edge, witch might cause trouble. The lower expression is corrected to give 
# zero thickness at the trailing edge, but the foil is strictly speaking no
# longer a proper NACA airfoil.
#
# See http://turbmodels.larc.nasa.gov/naca4412sep_val.html
#     http://en.wikipedia.org/wiki/NACA_airfoil

#z_t = (t*c/0.2) * (0.2969.*(x/c).^0.5 - 0.1260.*(x/c) - 0.3516.*(x/c).^2 + 0.2843.*(x/c).^3 - 0.1015.*(x/c).^4);
z_t = (t*c/0.2) * (0.2969*(x/c)**0.5 - 0.1260*(x/c) - 0.3516*(x/c)**2 + 0.2843*(x/c)**3 - 0.1036*(x/c)**4);


# Calculate camber
if (p > 0):
    # Calculate camber
    z_c = z_c + (m*x/p^2) * (2*p - x/c) * (x < p*c);
    z_c = z_c + (m*(c-x)/(1-p)^2) * (1 + x/c - 2*p) * (x >= p*c);

    # Calculate theta-value
    theta = theta + atan( (m/p^2) * (2*p - 2*x/c) ) * (x < p*c);
    theta = theta + atan( (m/(1-p)^2) * (-2*x/c + 2*p) ) * (x >= p*c);


# Calculate coordinates of upper surface
Xu = x - z_t* npy.sin(theta);
Zu = z_c + z_t*npy.cos(theta);

# Calculate coordinates of lower surface
Xl = x + z_t*npy.sin(theta);
Zl = z_c - z_t*npy.cos(theta);


# Rotate foil to specified angle of attack
upper = npy.dot( npy.array([ [ npy.cos(alpha), npy.sin(alpha)], [ -npy.sin(alpha), npy.cos(alpha)] ]) , npy.array([ Xu,  Zu ]) );
lower = npy.dot( npy.array([ [npy.cos(alpha), npy.sin(alpha)], [ -npy.sin(alpha), npy.cos(alpha)] ]) , npy.array([ Xl, Zl ]));


# Merge upper and lower surface (NB: Assume that the trailing edge is sharp)
# (see comments w.r.t. thickness calculation above)
X = [ upper[0,:], lower[0,Ni-1:-1:2] ];
Z = [ upper[1,:], lower[1,Ni-1:-1:2] ];
N = len(X[0]);
print N

xbb = X;
zbb = Z;

#%[X,Z] = square(50,0,0,.15,1,1);
#[X,Z] = circle(2*Ni,1,0,.15,1,1);
#N = length(X);


# Triangulate the end surface
#tri = npy.array([ [1, 2, N] ]);
tri = npy.array([ [0, 1, N-1] ]);
#for i=2:Ni-1
for i in range(1,Ni-1):
    #tri = [tri; i, i+1, N-i+2];
    tri= npy.append( tri,[[i, i+1, N-i+2]], axis=0 )

#for i=Ni+1:N-1
for i in range(Ni,N-1):
    #tri = [tri; i, i+1, N-i+2];
    tri = npy.append( tri,[[i, i+1, N-i+2]] , axis=0)
#end


#% Make it 3D
X = npy.array([ X[0], X[0] ]).transpose();
Z = npy.array([ Z[0], Z[0] ]).transpose();
Y = (s/2)*npy.ones(N) -(s/2)*npy.ones(N);


#% Triangulate the second end surface
#tri = [tri; tri[:,1]+N, tri[:,0]+N, tri[:,2]+N];
a = [[tri[:,1]+N, tri[:,0]+N, tri[:,2]+N]]
b = npy.array(a).reshape(Ni-1,3)
tri = npy.append( tri, b , axis=0 )



#% Triangulate the top and bottom
#for i=1:N-1
for i in range(0,N-1):
    #tri = [tri; i, N+i, i+1];
    tri = npy.append( tri , [[i,N+i,i+1]], axis=0)
#end
#tri = [tri; N, 2*N, 1];
tri = npy.append( tri, [[N,2*N,1]] , axis=0)

#for i=N+1:(2*N-1)
for i in range(N+1,2*N-1):
    #tri = [tri; i, i+1, i-N+1];
    tri = npy.append( tri , [[i, i+1, i-N+1]] , axis=0 )
#end
#tri = [tri; 2*N, N+1, 1];
tri = npy.append( tri , [[2*N, N+1, 1]] , axis=0)

#tri = npy.array(tri)

# Open file
#%fo = fopen('airfoil2.stl', 'w');
#fo = fopen('square.stl', 'w');


#% Write file
print 'solid airfoil\n';

#for i=1:length(tri)
for i in range( len(tri) ):
    #  % Calculate normal vector
    AB = [X[tri[i,1]] - X[tri[i,0]], Y[tri[i,1]] - Y[tri[i,0]], Z[tri[i,1]] - Z[tri[i,0]] ];
    AC = [X[tri[i,2]] - X[tri[i,0]], Y[tri[i,2]] - Y[tri[i,0]], Z[tri[i,2]] - Z[tri[i,0]] ];
    n = npy.matrix( npy.cross(AB, AC) ) / npy.matrix( npy.linalg.norm(npy.cross(AB, AC)));
  
    # Write facet
    print '  facet normal %e %e %e\n', n;
    print '    outer loop\n' ;
    print '      vertex %e %e %e\n', ( X[tri[i,0]], Y[tri[i,0]], Z[tri[i,0]] )  ;
    print '      vertex %e %e %e\n', ( X[tri[i,1]], Y[tri[i,1]], Z[tri[i,1]] )  ;
    print '      vertex %e %e %e\n', ( X[tri[i,2]], Y[tri[i,2]], Z[tri[i,2]] )  ;
    print '    endloop\n';
    print '  endfacet\n' ;


print 'endsolid airfoil\n';


#% Close file
#fclose(fo);
