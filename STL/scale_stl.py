import numpy as npy
import copy

#import pdb

# Import ASCII stl file, unit scale it (centered at 0 and length 1 in x)

#stl_file = "cylinder.stl"
#fid = open(stl_file,'r')
#lines = fid.readlines()
#fid.close()

def setName(stl_lines,name):
    stl_lines[0]  = "solid %s \n" % name
    stl_lines[-1] = "endsolid %s \n" % name
    return stl_lines
    
    

def getScales(stl_lines):

    # Get mean x,y,z of all points
    big = 1.0e9;
    xmean = 0; xmax = -big; xmin = big;
    ymean = 0; ymax = -big; ymin = big;
    zmean = 0; zmax = -big; zmin = big;
    vcnt = 0
    for line in stl_lines:
        if ( "vertex" in line ):
            vcnt += 1
            pts = line.strip().replace("vertex",'').strip().split(' ')
            x = float(pts[0])
            y = float(pts[1])
            z = float(pts[2])
            xmean += x
            ymean += y
            zmean += z
            xmin = min(xmin,x) 
            xmax = max(xmax,x)
            ymin = min(ymin,y) 
            ymax = max(ymax,y)
            zmin = min(zmin,z) 
            zmax = max(zmax,z)

    xmean /= vcnt
    ymean /= vcnt
    zmean /= vcnt
    
    return [xmean, ymean, zmean, xmin, xmax, ymin, ymax, zmin, zmax]
  

def setScales(stl_lines,xmin,xmax,ymin,ymax,zmin,zmax):

    # Get mean x,y,z of all points
    vcnt = 0
    stl_new = []
    for line in stl_lines:
        stl = copy.copy(line)
        if ( "vertex" in line ):
            vcnt += 1
            pts = stl.strip().replace("vertex",'').strip().split(' ')
            x = float(pts[0])
            y = float(pts[1])
            z = float(pts[2])      
            xs = ( x - xmin) / (xmax - xmin)   # Scale 0-1
            ys = ( y - ymin) / (ymax - ymin)
            zs = ( z - zmin) / (zmax - zmin) 
            stl = "  vertex  %s %s %s \n" % ( xs, ys, zs )
        stl_new.append(stl)
    
    return stl_new
      
def stl_scale(stl_lines,xscale,yscale,zscale):
    # Get mean x,y,z of all points
    vcnt = 0
    stl_new = []
    for line in stl_lines:
        stl = copy.copy(line)
        if ( "vertex" in line ):
            vcnt += 1
            pts = stl.strip().replace("vertex",'').strip().split(' ')
            x = float(pts[0])
            y = float(pts[1])
            z = float(pts[2])      
            xs = x * xscale
            ys = y * yscale
            zs = z * zscale
            stl = "  vertex  %s %s %s\n" % ( xs, ys, zs )
        stl_new.append(stl)
    
    return stl_new

def stl_rotate(stl_lines,alpha):
    # Get mean x,y,z of all points
    vcnt = 0
    stl_new = []
    for line in stl_lines:
        stl = copy.copy(line)
        if ( "vertex" in line ):
            vcnt += 1
            pts = stl.strip().replace("vertex",'').strip().split(' ')
            x = float(pts[0])
            y = float(pts[1])
            z = float(pts[2])      
            xs = x * npy.cos( alpha ) - z * npy.sin( alpha )
            ys = y
            zs = x * npy.sin( alpha ) + z * npy.cos( alpha )
            stl = "  vertex  %s %s %s\n" % ( xs, ys, zs )
        stl_new.append(stl)
    
    return stl_new

def stl_move(stl_lines,xoff,yoff,zoff):
    # Get mean x,y,z of all points
    vcnt = 0
    stl_new = []
    for line in stl_lines:
        stl = copy.copy(line)
        if ( "vertex" in line ):
            vcnt += 1
            pts = stl.strip().replace("vertex",'').strip().split(' ')
            x = float(pts[0])
            y = float(pts[1])
            z = float(pts[2])      
            xs = x + xoff
            ys = y + yoff
            zs = z + zoff
            stl = "  vertex  %s %s %s\n" % ( xs, ys, zs )
        stl_new.append(stl)
    
    return stl_new


def switchYZ(stl_lines):
    stl_new = []
    for line in stl_lines:
        stl = copy.copy(line)
        if ( "vertex" in line ):
            pts = stl.strip().replace("vertex",'').strip().split(' ')
            x = float(pts[0])
            y = float(pts[1])
            z = float(pts[2])      
            stl = "  vertex  %s %s %s\n" % ( x, z, y )
        stl_new.append(stl)
    
    return stl_new



def switchXY(stl_lines):
    stl_new = []
    for line in stl_lines:
        stl = copy.copy(line)
        if ( "vertex" in line ):
            pts = stl.strip().replace("vertex",'').strip().split(' ')
            x = float(pts[0])
            y = float(pts[1])
            z = float(pts[2])      
            stl = "  vertex  %s %s %s\n" % ( y, x, z )
        stl_new.append(stl)
    
    return stl_new


def centerSTL(stl_lines):
    [xmean, ymean, zmean, 
     xmin, xmax, ymin, 
     ymax, zmin, zmax] = getScales(stl_lines)
    
    xbar = (xmax+xmin)/2.0
    ybar = (ymax+ymin)/2.0
    zbar = (zmax+zmin)/2.0
    stl_new = stl_move(stl_lines,-xbar,-ybar,-zbar)
    return stl_new

import numpy as np
from struct import unpack
 
def BinarySTL(fname):
    fp = open(fname, 'rb')
    Header = fp.read(80)
    nn = fp.read(4)
    Numtri = unpack('i', nn)[0]
    #print nn
    record_dtype = np.dtype([
                   ('normals', np.float32,(3,)),  
                   ('Vertex1', np.float32,(3,)),
                   ('Vertex2', np.float32,(3,)),
                   ('Vertex3', np.float32,(3,)) ,              
                   ('atttr', '<i2',(1,) )
    ])
    data = np.fromfile(fp , dtype = record_dtype , count =Numtri)
    fp.close()
 
    Normals = data['normals']
    Vertex1= data['Vertex1']
    Vertex2= data['Vertex2']
    Vertex3= data['Vertex3']
 
    p = np.append(Vertex1,Vertex2,axis=0)
    p = np.append(p,Vertex3,axis=0) #list(v1)
    Points =np.array(list(set(tuple(p1) for p1 in p)))
     
    return Header,Points,Normals,Vertex1,Vertex2,Vertex3
  
#if __name__ == '__main__':
#    fname = "ship.stl" # "porsche.stl"
#    head,p,n,v1,v2,v3 = BinarySTL(fname)
#    print head
#    print p.shape


# Make unit dimensions centered at (0,0,0)
#[xmean, ymean, zmean, xmin, xmax, ymin, ymax, zmin, zmax] = getScales(lines)
#nlines = setScales(lines,xmin,xmax,ymin,ymax,zmin,zmax)
#xbar = (xmean-xmin)/(xmax-xmin)
#ybar = (ymean-ymin)/(ymax-ymin)
#zbar = (zmean-zmin)/(zmax-zmin)
#nlines = stl_move(nlines,-xbar,-ybar,-zbar)

# Scale, rotate and translate
#nlines = stl_scale(nlines,3.0, 1.0, 1.0)
#nlines = stl_rotate(nlines,-.1)
#nlines = stl_move(nlines,10.0,1.0,0.0)



#stl_file = "cylinder_unit.stl"
#fid = open(stl_file,'w')
#for line in nlines:
#    fid.write( line )
#fid.close()



