function [x,y] = square(npts,xc,yc,alpha,Lx,Ly)

% Make points describing a square

% 1) Make unit square
x = linspace(-1,1,npts);
x = [ x , ones( 1, npts-2) ];
x = [ x , linspace(1,-1,npts) ];
x = [ x , - ones( 1 , npts-2) ];

y = ones( 1, npts-1);
y = [ y , linspace(1,-1,npts) ];
y = [ y , - ones( 1 , npts-2) ];
y = [ y , linspace(-1,1,npts-1)];

% 2) Rotate and scale
x = x*Lx;
y = y*Ly;

dd = [cos(alpha), sin(alpha); -sin(alpha), cos(alpha)] * [x ; y];
x = dd(1,:);
y = dd(2,:);

% 3) Add offset
x = x + xc;
y = y + yc;


end
