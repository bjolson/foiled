import numpy as npy
import matplotlib.pyplot as plt
from copy import deepcopy as copy


# Make a general number of polygon inscribed in a unit circle.



def poly_stl(Rin, Nin, Zin ):


    R0 = Rin   # Radius of circle
    Np = int(Nin)    # Number of sides
    z1 = Zin   # Width of the z-direction

    faces = []

    dtheta = 2*npy.pi / Np;

    theta0 = dtheta / 2.0;
    if ( Np%2 == 1):
        theta0 = 0.0

    pt0 = [ R0*npy.cos(theta0) , R0 * npy.sin(theta0) , z1/2.0 ]
    pt2 = [0.0, 0.0, z1/2.0 ]

    # Front face
    for fc in range(Np):
        theta0 += dtheta
        pt1 = [ R0*npy.cos(theta0) , R0 * npy.sin(theta0) , z1/2.0 ]
        faces.append( [pt0, pt1, pt2] )
        pt0 = pt1

    # Back face
    for fc in range(Np):
        pts = copy(faces[fc])
        pt0 = pts[0]
        pt0[2] = -z1/2.
        pt1 = pts[1]
        pt1[2] = -z1/2.0
        pt2 = pts[2]
        pt2[2] = -z1/2.0    
        faces.append( [pt0,pt1,pt2] )

    # Join the faces
    for fc in range(Np):
        pts = copy(faces[fc])
        pt0 = pts[0]
        pt1 = pts[1]
        pt2 = [ pt0[0], pt0[1], -z1/2.0]
        faces.append( [pt0,pt1,pt2] )

    # Join the other faces
    for fc in range(Np):
        pts = copy(faces[fc])
        pt = pts[0]
        pt0 = [ pt[0], pt[1], -z1/2.0 ]
        pt = pts[1]
        pt1 = [ pt[0], pt[1], -z1/2.0 ]
        pt2 = pt
        faces.append( [pt0, pt1, pt2] )


    stl_lines = []

    stl_lines.append("solid poly-%s \n" % (Np) )
    for face in faces:
        stl_lines.append("facet normal 0.0 0.0 -1.0\n")
        stl_lines.append("  outer loop \n")
        for pt in face:
            stl_lines.append("  vertex %s %s %s \n" % (pt[0],pt[1],pt[2]))
        stl_lines.append("  endloop \n")
        stl_lines.append("endfacet \n")
    stl_lines.append("endsolid poly-%s \n" % (Np) )


    return stl_lines






#Np = 4.0
#stl_lines = poly_stl(1.0, Np , 1.0)

# Write the STL
#stl_file = 'poly-%s.stl' % Np
#fid = open(stl_file,'w')
#for line in stl_lines:
#    fid.write(line)
#fid.close()
