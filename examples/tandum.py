# Step 1
from foiler import *

# Step 2
tunnel = WindTunnel("cylinder")
tunnel.Uinf = 0.1
tunnel.Load()

# Step 3
cyl = WTshape("circle","myCircle")
cyl.args['scale'] = [1,1,1]
cyl.args['center'] = [0,-.5,0.0]
cyl.wake = True
tunnel.addShape(cyl)

cyl2 = WTshape("circle","myCircle2")
cyl2.args['scale'] = [1,1,1]
cyl2.args['center'] = [3,-.5,0.0]
cyl2.wake = True
tunnel.addShape(cyl2)


# Step 4
tunnel.makeHexMesh()
tunnel.showMesh()

# Step 5
tunnel.runSimple(100)

# Step 6
ftime = tunnel.NVizStates
tunnel.visitDriver('Field',simpleFoam,'umag',[ftime])
tunnel.showField(ftime)

# Step 7 - Lift/Drag and pressure vs. X
[x,p] = tunnel.surfacePressure('myCircle',ftime)

tunnel.Save()

# Step 8
[D,L] = tunnel.getForces('myCircle',ftime)
print "Lift: %s" % L
print "Drag: %s" % D

import matplotlib.pyplot as plt
#plt.plot(x,p,'k--')
#plt.show()

LT = tunnel.getLift('simple','myCircle')
DT = tunnel.getDrag('simple','myCircle')
LT2 = tunnel.getLift('simple','myCircle2')
DT2 = tunnel.getDrag('simple','myCircle2')
