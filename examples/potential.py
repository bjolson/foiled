# Step 1
from foiler import *
import matplotlib.pyplot as plt

# Step 2
tunnel = WindTunnel("test_potential")
tunnel.Uinf = 0.1
tunnel.Load()

# Step 3
tri = WTshape("circle","tri")
tri.args['scale'] = [2,1,.5]
tri.args['center'] = [3,-.5,0.0]
tri.args['alpha'] = -.1
tunnel.addShape(tri)

# Step 4
tunnel.simpleTviz = 1
tunnel.makeHexMesh()
tunnel.showMesh()


tunnel.runPotential(1)
tunnel.visitDriver('Field',potentialFoam,'Pot')
tunnel.showField(0,'Pot')

