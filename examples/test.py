# Step 1
import foiler

# Step 2
tunnel = foiler.foiled("cylinder")
tunnel.workpath = '.' 
tunnel.setupWorkDir()
tunnel.Uinf = 0.1

# Step 3
cyl = foiler.shape("circle","myCircle")
cyl.args['scale'] = [1,1,1]
cyl.args['center'] = [0,-.5,0.0]
tunnel.addShape(cyl)

# Step 4
tunnel.makeHexMesh()
tunnel.showMesh()

# Step 5
tunnel.runSimple(100)

# Step 6
tunnel.visitDriver('Field','simple','umag',[1,5,10])
tunnel.showField(10)

# Step 7 - Lift/Drag and pressure vs. X
tunnel.visitDriver('Forces','simple','p',[10],shape='myCircle')
[x,p] = tunnel.surfacePressure('myCircle',10)


# Step 8
[D,L] = tunnel.getForces('myCircle',10)
print "Lift: %s" % L
print "Drag: %s" % D

import matplotlib.pyplot as plt
plt.plot(x,p,'k--')
plt.show()
