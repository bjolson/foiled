# Step 1
from foiler import *

# Step 2
tunnel = WindTunnel("cylinder")
tunnel.Uinf = 0.1

# Step 3
cyl = WTshape("circle","myCircle")
cyl.args['scale'] = [1,1,1]
cyl.args['center'] = [0,-.5,0.0]
tunnel.addShape(cyl)

# Step 4
tunnel.makeHexMesh()
tunnel.showMesh()

# Step 5
tunnel.runSimple(100)

# Step 6
tunnel.visitDriver('Field','simple','umag',[1,5,10])
tunnel.showField(10)

# Step 7 - Lift/Drag (using visit)
tunnel.visitDriver('Forces','simple','p',[10],shape='myCircle')
#-- This is net Lift & Drag at a time
[D,L] = tunnel.getForces('myCircle',10)
print "Lift: %s" % L
print "Drag: %s" % D


# Step 8 - Net lift and drag vs. time (useful for residual/convergence)
LT = tunnel.getLift('simple','myCircle')
DT = tunnel.getDrag('simple','myCircle')

# Step 9 - Plot the L/D vs. time/cycle
import matplotlib.pyplot as plt
plt.plot( LT[:,0], LT[:,1] )
plt.hold('on')
plt.plot( DT[:,0], DT[:,1] )
plt.show()
