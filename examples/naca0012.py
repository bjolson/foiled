# Step 1
from foiler import *
import matplotlib.pyplot as plt

# Step 2
tunnel = WindTunnel("naca_airfoil")
tunnel.Uinf = 0.1
tunnel.Load()

# Step 3
naca = WTshape("NACA","naca")
naca.args['scale'] = [4,4,4]
naca.args['center'] = [3,-.5,0.0]
naca.args['Number'] = [0,0,1,2]
naca.args['alpha'] = -.1
naca.wake = True
tunnel.addShape(naca)

# Step 4
tunnel.makeHexMesh()
tunnel.showMesh()

# Step 5
tunnel.runSimple(200)

# Step 6
ftime = tunnel.NVizStates
tunnel.visitDriver('Field',simpleFoam,'umag',[ftime])
tunnel.showField(ftime)

# Step 7 - Lift/Drag and pressure vs. X
[x,p] = tunnel.surfacePressure('naca',ftime)
plt.figure(1)
plt.plot(x,p,'k--')
plt.xlabel('x')
plt.ylabel('P')
tunnel.Save()

# Step 8 - Plot L/D vs time
LT2 = tunnel.getLift('simple','naca')
DT2 = tunnel.getDrag('simple','naca')
plt.figure(2)
plt.plot(LT2[:,0],LT2[:,1]/DT2[:,1])
plt.xlabel('Time')
plt.ylabel('L/D')
plt.show()
