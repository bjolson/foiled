# Step 1
from foiler import *
import matplotlib.pyplot as plt

# Step 2
tunnel = WindTunnel("test_triangle")
tunnel.Uinf = 0.1
tunnel.Load()

# Step 3
tri = WTshape("triangle","tri")
tri.args['scale'] = [1,1,1]
tri.args['center'] = [3,-.5,0.0]
tri.args['alpha'] = -.1
tunnel.addShape(tri)

tri2 = WTshape("triangle","tri2")
tri2.args['scale'] = [1,1,1]
tri2.args['center'] = [4,-.5,1.5]
tri2.args['alpha'] = .2
tunnel.addShape(tri2)

# Step 4
tunnel.simpleTviz = 1
tunnel.makeHexMesh()
tunnel.showMesh()

for i in range(3):
    # Step 5
    tunnel.runSimple(10)

    # Step 6
    ftime = tunnel.NVizStates
    tunnel.visitDriver('Field',simpleFoam,'umag', [ftime] )
    tunnel.showField(ftime)
    tunnel.Save()



# Step 8 - Plot L/D vs time
LT2 = tunnel.getLift('simple','tri')
DT2 = tunnel.getDrag('simple','tri')
plt.figure(2)
plt.plot(LT2[:,0],LT2[:,1]/DT2[:,1])
plt.xlabel('Time')
plt.ylabel('L/D')
plt.show()
