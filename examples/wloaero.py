import os
import sys
import numpy as npy
import matplotlib.pyplot as plt
import foiler


ff = foiler.foiled("medium")
ff.workpath = '.'
ff.setupWorkDir()

ff.Load()


# Add Shapes to the windtunnel
iscale = 1.0/5.0

upper = foiler.shape("user","upper")
upper.args['file'] =  foiler.STL_DIR + "BottomWing.stl"  
upper.args['swap'] = "YZ"
upper.args['scale'] =  [ iscale, iscale, iscale ]
upper.args['center'] = [ 5, 0, -0.0]
upper.args['alpha'] = -0.00
ff.addShape(upper)

lower = foiler.shape("user","lower")
lower.args['file'] = foiler.STL_DIR + "BottomWing.stl"  
lower.args['swap'] = "YZ"
lower.args['scale'] =  [ iscale, iscale, iscale ]
lower.args['center'] = [ 5, 0, -2.0]
lower.args['alpha'] = -0.00
ff.addShape(lower)

disk = foiler.shape("circle","disk")
disk.args['scale'] = [10, 1, .25]
disk.args['center'] = [5, 0, -4]
disk.args['alpha'] = -0.00
ff.addShape(disk)

# Shape refinement settings
ff.stlLevelMin = 2
ff.stlLevelMax = 2
ff.stlLevelEdge = 3

# Block mesh settings
ff.block = [-6, 16, -.2, .2, -8, 8]
ff.blockNX = 240 #120
ff.blockNY = 120 #60
ff.locationInMesh = [-5,-.1,0]

# Make the mesh
ff.makeHexMesh()
ff.Save()

ff.simpleTviz = 100
ff.runSimple(200)
ff.Save()

ff.visitUpdate('simple')

# Pressure over foil
vt = ff.simpleCurrentTime / ff.simpleTviz
ff.visitDriver('Forces','simple','umag',[vt],shape='upper')
[x,p] = ff.surfacePressure('upper',vt,solver='simple')
plt.plot(x,p,'k');plt.hold('on')

ff.visitDriver('Forces','simple','umag',[vt],shape='lower')
[x,p] = ff.surfacePressure('lower',vt,solver='simple')
plt.plot(x,p,'b')

