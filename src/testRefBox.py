import os

import foiler
ff = foiler.foiled("test")
ff.workpath = os.path.abspath('../tmp/poop3')
ff.setupWorkDir()

ss = foiler.shape("circle","circle-1")
ss.args['scale'] = [1,1,1]
ss.args['scale'] = [3,1,1]
ss.args['center'] = [0,-.5,0.0]
ss.args['alpha'] = -0.3

ff.addShape(ss)


air = foiler.shape("NACA","naca-1")
air.args['scale'] = [5,1,5]
air.args['center'] = [9,0.0,0.0]
air.args['alpha'] = -0.1
air.args['Number'] = [2,4,1,5]

ff.addShape(air)

# Add refinement box/ mesh settings
box = foiler.refinementBox()
box.min = [-1, -1, -1]
box.max = [10,  1,  1]
box.levels = [1e15, 1]
ff.addRefineBox(box)

box1 = foiler.refinementBox()
box1.min = [9,  -1,  -2]
box1.max = [16,   1,   2]
box1.levels = [1e15, 1]
ff.addRefineBox(box1)


ff.stlLevelMin = 0
ff.stlLevelMax = 1
ff.stlLevelEdge = 1


ff.writeSTL()

ff.block = [-6, 16, -.2, .2, -8, 8]
ff.blockNX = 120
ff.blockNY = 60
#ff.setBlockMesh()

ff.locationInMesh = [-5,-.1,0]

ff.simpleTmax = 600
ff.simpleTviz = 10

ff.makeHexMesh()

# Plot the Mesh
ff.visitUpdate('simple')
ff.visitDriver('Mesh','simple','Mesh')

ff.runFoam('Simple')
ff.visitUpdate('simple')
ff.visitDriver('Field','simple','umag',[1,2,3,4,5,6,7,8,9])
#ff.visitDriver('Field','simple','umag',range(1,60))
