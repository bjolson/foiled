## These scripts/routines are run in Visit CLI
import os
import sys


def airfoilSlice(inter,idir='y'):    
    AddOperator("Slice")
    slc = SliceAttributes()
    slc.originIntercept = inter
    if 'x' in idir:
        slc.axisType = slc.XAxis
    if 'y' in idir:
        slc.axisType = slc.YAxis
    if 'z' in idir:
        slc.axisType = slc.ZAxis
    SetOperatorOptions(slc)

def clearAnnotations():
    return

def saveImage(filename,xres,yres):
    sw = SaveWindowAttributes()
    sw.family = 0
    sw.format = sw.JPEG
    sw.width  = xres
    sw.height = yres
    sw.outputToCurrentDirectory = 0
    sw.outputDirectory = os.path.dirname(filename)
    sw.fileName = os.path.basename(filename)
    SetSaveWindowAttributes(sw)
    SaveWindow()
    return

        
def plotMesh(DB,outfile):

    OpenDatabase(DB)
    AddPlot("Mesh",'mesh')
    airfoilSlice(-.25, 'y')
    DrawPlots()

    saveImage(outfile,2048,2048)

    return

def launchVisit(DB,field):
    OpenDatabase(DB)
    AddPlot("Pseudocolor",field)
    airfoilSlice(-.25,'y')
    endTime = TimeSliderGetNStates() - 1
    SetTimeSliderState(endTime)
    DrawPlots()
    OpenGUI()
    return

def plotField(DB,outfile,field,times):
    OpenDatabase(DB)
    AddPlot("Pseudocolor",field)
    airfoilSlice(-.25, 'y')

    if times:
        for time in times:
            ofile = outfile + '_' + str(time).zfill(3)
            SetTimeSliderState(time)
            DrawPlots()
            saveImage(ofile,2048,2048)
    else:
        ofile = outfile + '_' + str(0).zfill(3)
        DrawPlots()
        saveImage(ofile,2048,2048)
    

    return


def plotForces(DB,outfile,times,force="lift"):
    OpenDatabase(DB)
    DefineVectorExpression('normal','point_surface_normal(mesh)')
    DefineScalarExpression('nx','normal[0]')
    DefineScalarExpression('ny','normal[2]')
    DefineScalarExpression('x','coords(mesh)[0]')
    DefineScalarExpression('y','coords(mesh)[2]')
    DefineScalarExpression('Fx','p*nx*area(mesh)')
    DefineScalarExpression('Fy','p*ny*area(mesh)')

    sa = ScatterAttributes()
    sa.var1 = "x"
    sa.var1Role = sa.Coordinate0
    sa.pointSizePixels = 5
    if (force == "lift"):
        sa.var2 = "Fy"
        AddPlot("Pseudocolor","Fy")
    elif (force == "drag"):
        sa.var2 = "Fx"
        AddPlot("Pseudocolor","Fx")
    else:
        print "Error: %s is not a valid force" % (force)
        return []

    #AddPlot("Scatter",sa.var2)
    #SetPlotOptions(sa)
    #SetActivePlots((0,1))
    #HideActivePlots()
    airfoilSlice(-.25, 'y')    
    
    dirname = os.path.dirname(DB)
    DrawPlots()
    F = []
    for time in times:
        ofile = outfile + '_' + str(time).zfill(3)
        SetTimeSliderState(time)

        # Get the total force
        #SetActivePlots(0)
        #HideActivePlots()  # On
        DrawPlots()
        Query("Variable Sum")
        F.append( GetQueryOutputValue() )
        #HideActivePlots()   # Off

        # Save curve variables over surface
        exd = ExportDBAttributes()
        exd.allTimes = 0
        exd.db_type = "Curve2D"
        exd.variables = ('p','x','y','nx','ny','Fy','Fx')
        exd.dirname = dirname
        exd.filename = 'surface_' + str(time).zfill(3)
        
        ExportDatabase(exd)


    return F



### Main driver for visit scripts
visitDB     = "##visitDB##"
visitFile   = "##visitFile##"
visitScript = "##visitScript##"
visitTimes = ##visitTimes##


# Visit output cases
if ( visitScript == 'Mesh' ):
    plotMesh(visitDB,visitFile)
    exit()

if ( visitScript == 'Field' ):
    field = "U_magnitude"
    plotField(visitDB,visitFile,field,visitTimes)
    exit()

if ( visitScript == 'Forces' ):
    field = "U_magnitude"
    print plotForces(visitDB,visitFile,visitTimes,force="lift")
    exit()

if ( visitScript == "Launch" ):
    field = "U_magnitude"
    launchVisit(visitDB,field)
    exit()


# Shutdown Visit





