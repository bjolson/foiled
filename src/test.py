import os

import foiler
ff = foiler.foiled("test")
ff.workpath = os.path.abspath('../tmp/poop2')
ff.setupWorkDir()
ss = foiler.shape("circle","circle-1")
ss.args['scale'] = [1,1,1]
ss.args['scale'] = [3,1,1]
#ss.args['center'] = [0,-.5,0.0]
ss.args['alpha'] = -0.3

#ff.addShape(ss)


##ss1 = foiler.shape("circle","circle-2")
##ss1 = foiler.shape("square","square-1")
#ss1 = foiler.shape("triangle","triangle-1")
#ss1.args['scale'] = [1,1,1]
#ss1.args['center'] = [9,0.0,0.0]
#ss1.args['alpha'] = -0.1

air = foiler.shape("NACA","naca-1")
air.args['scale'] = [5,1,5]
air.args['center'] = [9,0.0,0.0]
air.args['alpha'] = -0.1

#ff.addShape(air)


#air2 = foiler.shape("NACA","naca-2")
#air2.args['scale'] = [5,1,5]
#air2.args['center'] = [9,0.0,4.0]
#air2.args['alpha'] = -0.1
#ff.addShape(air2)

moon = foiler.shape("user","mooney")
moon.args['file'] = "../STL/mooney.stl"
moon.args['swap'] = "XY"
moon.args['scale'] = [6,1,6]
moon.args['center'] = [4,-0.2,0.0]
#ff.addShape(moon)

wlo = foiler.shape("user","seaplane")
wlo.args['file'] = "../STL/wloaero.stl"
wlo.args['swap'] = "YZ"
wlo.args['scale'] = [1./200.,1./200.,1./200.]
wlo.args['center'] = [4,-2.2,0.0]
ff.addShape(wlo)


# Add refinement box/ mesh settings
box = foiler.refinementBox()
box.max = [10, 1, 1]
box.levels = [1e15, 2]
ff.addRefineBox(box)

ff.stlLevelMin = 1
ff.stlLevelMax = 2
ff.stlLevelEdge = 3

#ff.getSTL()

ff.writeSTL()

ff.block = [-6, 16, -.2, .2, -8, 8]
ff.blockNX = 240
ff.blockNY = 120
#ff.setBlockMesh()

ff.locationInMesh = [-5,-.1,0]

ff.simpleTmax = 600
ff.simpleTviz = 10

ff.makeHexMesh()

# Plot the Mesh
ff.visitUpdate('simple')
ff.visitDriver('Mesh','simple','Mesh')

#ff.runFoam('Simple')
#ff.visitUpdate('simple')
##ff.visitDriver('Field','simple','umag',[1,2,3,4,5,6,7,8,9])
#ff.visitDriver('Field','simple','umag',range(1,60))
