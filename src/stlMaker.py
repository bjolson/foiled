import numpy as npy
import sys
import os
import copy
import subprocess
from fart import farts
import shutil
import cPickle
import Image
import matplotlib.pyplot as plt
import glob

SRC_DIR = os.path.dirname(os.path.realpath(__file__))
STL_DIR = "%s/../STL/" % SRC_DIR
DAT_DIR = "%s/../rundata/airfoil-base/*" % SRC_DIR

# Integer for solver flags
potentialFoam = 'potential'
simpleFoam = 'simple'
pimpleFoam = 'pimple'

sys.path.append(STL_DIR)
import scale_stl as stllib
from poly_stl import poly_stl



from foiler import WTshape

class stlmaker(WTshape):
    pass


def writeSTL(stl_lines,stlfile):
        fid = open(stlfile,'w')
        fid.write(stl_lines)
        fid.close()




circ = stlmaker('circle','MyCircle')
circ.args['scale'] = [1,1,.1]
circ.args['alpha'] = -.1
circ.getSTL()
writeSTL(circ.stl,'circ.stl')


circ = stlmaker('poly','MyPoly')
circ.args['sides'] = 400
circ.args['scale'] = [1,1,.1]
circ.args['alpha'] = -.1
circ.getSTL()
writeSTL(circ.stl,'poly.stl')

naca = stlmaker('NACA','myNaca')
naca.workpath = '.'
naca.args['Number'] = [0,0,1,2]
naca.args['alpha'] = -.15
naca.getSTL()
writeSTL(naca.stl,'naca0012.stl')


circ = stlmaker('circle','MyCircle')
circ.args['scale'] = [.2,.2,.2]
circ.args['center'] = [0,0,.5]
circ.getSTL()
writeSTL(circ.stl+naca.stl,'circNaca.stl')

