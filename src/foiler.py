import numpy as npy
import sys
import os
import copy
import subprocess
from fart import farts
import shutil
import cPickle
import Image
import matplotlib.pyplot as plt
import glob

SRC_DIR = os.path.dirname(os.path.realpath(__file__))
STL_DIR = "%s/../STL/" % SRC_DIR
DAT_DIR = "%s/../rundata/airfoil-base/*" % SRC_DIR

# Integer for solver flags
potentialFoam = 'potential'
simpleFoam = 'simple'
pimpleFoam = 'pimple'

sys.path.append(STL_DIR)
import scale_stl as stllib
from poly_stl import poly_stl


## Make a class that contains the entire problem setup
## Only 2d windtunnel like problems for now
class foiled():

    def __init__(self,name,fpath='.'):
        self.problemName = name
        self.block = [-6, 16, -.2, .2, -8, 8]
        self.blockNX = 220
        self.blockNY = 120
        self.blockNZ = 1
        self.spacing = []
        self.shapes = []
        self.refine = []
        self.stlLevelMin = 1
        self.stlLevelMax = 1
        self.stlLevelEdge = 2
        self.workpath = ''
        self.basepath = fpath
        self.stl = ''
        self.stopRun = 0
        self.visFreq = 10
        self.locationInMesh = []

        self.potentialTmax = 1000;
        self.potentialTviz = 10;
        self.potentialCurrentTime = 0;

        self.simpleTmax = 1000;
        self.simpleTviz = 10;
        self.simpleCurrentTime = 0;
        self.dumpfile = ''
        self.meshpic = ''
        self.Uinf = 2.0
        self.NVizStates = 0
        
        # Setup the workpath
        self.setupWorkDir()


    def Load(self):
        try:
            f = open(self.dumpfile,'rb')
            tmp_dict = cPickle.load(f)
            f.close()          
            self.__dict__.update(tmp_dict) 
            print "Restarting from %s" % (self.dumpfile)
        except:
            print "Warning: no restart file found"

        
    def Save(self):
        f = open(self.dumpfile,'wb')
        cPickle.dump(self.__dict__,f,2)
        f.close()

    def addShape(self,shape):    
        shape.workpath = self.workpath
        for ss in self.shapes:            
            if (shape.name in ss.name):
                print """
                Warning: shape name (%s) already exists.
                Not added.""" % (ss.name)
                return
        self.shapes.append(shape)
        
    def addRefineBox(self,box):
        N = len(self.refine)
        box.name = "refinementBox-%s" % N
        self.refine.append(box)

    def getSTL(self):        

        # Add up all the stl files in this geometry
        stl = ''
        for ss in self.shapes:
            ss.getSTL()
            stl += ss.stl + '\n'
        self.stl = stl

    def writeSTL(self):

        # Gather all the stl strings for the shapes
        self.getSTL()

        # Write the stl desc. to file
        relpath = '/airfoil_snappyHexMesh/constant/triSurface/'
        stlfile = 'airfoil.stl'
        path = self.workpath + relpath + stlfile
        fid = open(path,'w')
        fid.write(self.stl)
        fid.close()
        
    def setupWorkDir(self):
        self.workpath = os.path.abspath(self.basepath) + "/" + self.problemName
        self.dumpfile = self.workpath+'/%s.res' % self.problemName

        if (not os.path.exists(self.workpath)):
            os.makedirs(self.workpath)
            cmd = "cp -r %s %s;" % (DAT_DIR,self.workpath)
            subprocess.call(cmd, shell=True)



    # Set the blockMesh parameters for the problem
    def setBlockMesh(self):
        relpath = '/airfoil_snappyHexMesh/constant/polyMesh/'
        bfile = 'blockMeshDict'
        bmesh = self.workpath + relpath + bfile
        
        # Set extends of the overall domain
        bdict = {}
        bdict['#X1#'] = self.block[0]
        bdict['#XN#'] = self.block[1]
        bdict['#Y1#'] = self.block[2]
        bdict['#YN#'] = self.block[3]
        bdict['#Z1#'] = self.block[4]
        bdict['#ZN#'] = self.block[5]
        
        # Set resolution of background mesh
        bdict['#NDX#'] = self.blockNX
        bdict['#NDY#'] = self.blockNY
        bdict['#NDZ#'] = self.blockNZ
        

        farts( bmesh, bdict )


    def setSnappyMesh(self):
        relpath = '/airfoil_snappyHexMesh/system/'
        sfile = 'snappyHexMeshDict'
        smesh = self.workpath + relpath + sfile
        
        # Set extends of the overall domain
        sdict = {}
        sdict['#LIMX#'] = self.locationInMesh[0]
        sdict['#LIMY#'] = self.locationInMesh[1]
        sdict['#LIMZ#'] = self.locationInMesh[2]
        
        # Refinement boxes
        sdict["#REFBDEF#"] = RefBoxDef(self.refine)
        sdict["#REFBSET#"] = RefBoxSet(self.refine)

        # Refinement Levels for STL geometry
        sdict["#stlLevelEdge#"] = self.stlLevelEdge
        sdict["#stlLevelMin#"] =  self.stlLevelMin
        sdict["#stlLevelMax#"] =  self.stlLevelMax

        farts( smesh, sdict )

    def setWakes(self):
        
        for shape in self.shapes:
            if shape.wake:
                stl_lines = shape.stl.split('\n')
                [xb,yb,zb,xmin,xmax,ymin,ymax,zmin,zmax] = stllib.getScales(stl_lines)
                xmax = self.block[1]
                zmin *= 1.2
                zmax *= 1.2
                rbox = refinementBox()
                rbox.min = [xmin, ymin, zmin]
                rbox.max = [xmax, ymax, zmax]
                
                self.addRefineBox(rbox)
        

    def makeHexMesh(self):

        self.writeSTL()

        self.setWakes()

        # Check to see in location in mesh is set; else use upper-left
        if (not self.locationInMesh):
            self.locationInMesh = [self.block[0],0.0,self.block[-1]]
            print "Notice: No outside point was specified; using %s" % self.locationInMesh

        if not self.meshpic:
            # Modify the files needed for HexMesh
            self.setBlockMesh()
            self.setSnappyMesh()
            self.setSimpleFoam()
            
            # Run the snappyHexMesh Solver
            rundir = self.workpath
            print "Making Mesh...\n"
            retvar = os.getcwd()
            os.chdir(self.workpath)
            cmd = './MakeMesh'
            subprocess.call(cmd, shell=True)
            os.chdir(retvar)

            # Plot the mesh and update the meshpic file
            self.visitUpdate('simple')
            self.visitDriver('Mesh','simple','Mesh')
            self.meshpic = self.workpath + '/airfoil_images/Mesh.jpeg'
            
        else:
            print """
            Mesh is already created: To reset type:
            self.meshpic = ''
            Then try mesh command again."""

    def showMesh(self,size=600):
        im = Image.open(self.meshpic)
        im.thumbnail((size,size))
        im.show()

    def showField(self,time,field='umag',solver='simple',size=600):
        img_src = self.workpath + '/airfoil_images/%s_%s.jpeg' % (field,str(time).zfill(3) )
        im = Image.open(img_src)
        im.thumbnail((size,size))
        im.show()

    def resetPotentialFoam(self):
        relpath = '/airfoil_potentialFoam/system/'
        sfile = 'controlDict'
        target = self.workpath + relpath + sfile
        src = os.path.abspath(os.path.dirname(DAT_DIR)) + relpath + sfile

        cmd = "cp -r %s %s;" % (src,target)
        subprocess.call(cmd, shell=True)
        self.setPotentialFoam()
        

    def setPotentialFoam(self):
        relpath = '/airfoil_potentialFoam/system/'
        sfile = 'controlDict'
        smesh = self.workpath + relpath + sfile
        
        # Set simple foam solver attributes
        sdict = {}
        sdict['#TMAX#'] = self.potentialTmax;
        sdict['#TVIZ#'] = self.potentialTviz;
        farts( smesh, sdict )

        # Set free stream velocity
        relpath = '/airfoil_potentialFoam/0.org/'
        svel = self.workpath + relpath + 'U'
        sdict = {}
        sdict['#UINF#'] = self.Uinf
        farts( svel, sdict )
        farts( svel.replace('.org','') , sdict)

        
    def runPotential(self,tsteps):
        self.potentialTmax = self.potentialCurrentTime + tsteps
        self.resetPotentialFoam()
        print "Running potentialFoam for %s timesteps" % tsteps    
        self.runFoam('Potential')
        self.potentialCurrentTime = self.simpleTmax
        print "Total steps: %s" % self.potentialCurrentTime
        self.visitUpdate('potential')


    def resetSimpleFoam(self):
        relpath = '/airfoil_simpleFoam/system/'
        sfile = 'controlDict'
        target = self.workpath + relpath + sfile
        src = os.path.abspath(os.path.dirname(DAT_DIR)) + relpath + sfile

        cmd = "cp -r %s %s;" % (src,target)
        subprocess.call(cmd, shell=True)
        self.setSimpleFoam()
        

    def setSimpleFoam(self):
        relpath = '/airfoil_simpleFoam/system/'
        sfile = 'controlDict'
        smesh = self.workpath + relpath + sfile
        
        # Set simple foam solver attributes
        sdict = {}
        sdict['#TMAX#'] = self.simpleTmax;
        sdict['#TVIZ#'] = self.simpleTviz;
        farts( smesh, sdict )

        # Set free stream velocity
        relpath = '/airfoil_simpleFoam/0.org/'
        svel = self.workpath + relpath + 'U'
        sdict = {}
        sdict['#UINF#'] = self.Uinf
        farts( svel, sdict )


        # Set force coefficients
        self.setForces(solver="simple")

        
    def runSimple(self,tsteps):
        self.simpleTmax = self.simpleCurrentTime + tsteps
        self.resetSimpleFoam()
        print "Running simpleFoam for %s timesteps" % tsteps    
        self.runFoam('Simple')
        self.simpleCurrentTime = self.simpleTmax
        print "Total steps: %s" % self.simpleCurrentTime
        self.visitUpdate('simple')

    def runFoam(self,solver):
        relpath = self.workpath
        retvar = os.getcwd()
        os.chdir(relpath)
        cmd = './Run%s' % ( solver )
        subprocess.call(cmd, shell=True)
        os.chdir(retvar)
    

    def setPimpleFoam(self):
        relpath = '/airfoil_pimpleFoam/'
        sfile = 'control_dict'
        smesh = self.workpath + relpath + sfile
        
        # Set simple foam solver attributes
        sdict = {}
        
        farts( smesh, sdict )


    # Compute how many valid viz (visit) files are available
    def getNVizStates(self,solver):
        relpath = self.workpath + '/airfoil_%sFoam/' % (solver)
        vtk = glob.glob(relpath+'/VTK/*.vtk')
        self.NVizStates = len(vtk) - 1

        
    def visitUpdate(self,solver):        
        relpath = self.workpath + '/airfoil_%sFoam/' % (solver)
        print "Converting to VTK...\n"

        # Get latest data dumpfile
        vtk = glob.glob(relpath+'/VTK/*.vtk')
        v = 0
        for iv in vtk:
            nn = int(os.path.basename(iv).split('.vtk')[0].split('_')[-1])
            v = max(v,nn)
        retvar = os.getcwd()
        os.chdir( relpath )
        print v
        if (v == 0):
            cmd = "foamToVTK"
        else:
            cmd = "foamToVTK -time '%s:' " % (v+1)
        pipe = " > %s/airfoil_%sFoam/VTK.out" % (self.workpath,solver)
        subprocess.call(cmd+pipe, shell=True)
        os.chdir(retvar)

        # Special renaming or single shape cases
        if ( len(self.shapes) == 1):
            os.chdir( relpath + '/VTK' )
            name = self.shapes[0].name 
            dst = 'airfoil_%s' % name            
            try:
                os.makedirs(dst)
            except:
                pass
            vtks = glob.glob('airfoil/*')
            for vt in vtks:                
                shutil.copyfile(vt,vt.replace('airfoil',dst))
            os.chdir( dst )
            files = glob.glob('*.vtk')
            for f in files:
                ff = os.path.basename(f)
                if not (name in ff):
                    dst = ff.replace('airfoil_','airfoil_%s_' % name)
                    os.rename(ff,dst)
            os.chdir(retvar)

        # Update the Viz State
        self.getNVizStates(solver)
        
    # Visualize the mesh and save an image to file
    def visitDriver(self,visitScript,solver,visitFile,visitTimes=[],shape=''):
        # Visit Driver Script
        driverSrc = SRC_DIR + '/visit_scripts.py'
        driverPath = self.workpath + '/visit_scripts.py'

        if 'Launch' in visitScript:
            visitTimes = [0]

        # Database
        if not shape:
            if visitTimes:
                visitDB = self.workpath + '/airfoil_%sFoam/VTK/airfoil_%sFoam_*.vtk database' % (solver,solver)
            else:
                visitDB = self.workpath + '/airfoil_%sFoam/VTK/airfoil_%sFoam_0.vtk' % (solver,solver)
        else:
            if visitTimes:
                visitDB = self.workpath + '/airfoil_%sFoam/VTK/airfoil_%s/airfoil_%s_*.vtk database' % (solver,shape,shape)
            else:
                visitDB = self.workpath + '/airfoil_%sFoam/VTK/airfoil_%s/airfoil_%s_0.vtk' % (solver,shape,shape)
            
        
        # Images root
        images = self.workpath + '/airfoil_images/'
        
        # Copy base file over again to reset
        shutil.copy(driverSrc,driverPath)

        # Case specific settings
        visitFile = images + visitFile

        # Populate the driver with arguments and
        #   run visit cli on it        
        sdict = {}
        sdict['##visitDB##'] = visitDB
        sdict['##visitFile##'] = visitFile
        sdict['##visitScript##'] = visitScript
        sdict['##visitTimes##'] = '[]'
        sdict['##visitShape##'] = shape
        sdict['##visitArg1##'] = ''
        sdict['##visitArg2##'] = ''
        
        if visitTimes:
            sdict['##visitTimes##'] = str(visitTimes)
                    
        # Populate arguments in file
        farts(driverPath,sdict)

        # Actually run the visit script
        win = " -nowin "
        if ("Launch" in visitScript):
            win = ''
        cmd = "visit -cli %s -s %s" % (win,driverPath)
        pipe = " > %s/airfoil_%sFoam/Visit.out" % (self.workpath,solver)
        subprocess.call(cmd+pipe, shell=True)
         
    def setForces(self,solver="simple"):
        dirname = self.workpath + '/airfoil_%sFoam/system/' % solver
        cfile = dirname + "forceCoeffs"
        cdfile = dirname + "controlDict"
        # For single shape, just fart the file
        if ( len( self.shapes ) <= 1 ):
            ddict = {}
            ddict['##NAME##'] = ''
            ddict['##PATCH##'] = 'airfoil.*'
            farts(cfile,ddict)
            cdict = {}
            cdict['##INCLUDES##'] = '#include "forceCoeffs"'
            farts(cdfile,cdict)
            
        else: # Copy a new file for each shape
            includes = ''
            for shape in self.shapes:                
                name = shape.name
                src = cfile
                dst = cfile + "_" + name
                shutil.copy(src,dst)
                ddict = {}
                ddict['##NAME##'] = '_' + name
                ddict['##PATCH##'] = 'airfoil_' + name
                includes += '#include "forceCoeffs_%s" \n' % (name)
                farts(dst,ddict)
            # Now set the proper includes
            cdict = {}
            cdict['##INCLUDES##'] = includes
            farts(cdfile,cdict)

    def getForcesFOAM(self,solver,shape):
        if ( len(self.shapes) == 1 ):
            forceDir = self.workpath + '/airfoil_%sFoam/postProcessing/forces' % (solver)
            runs = glob.glob(forceDir + '/*')
            for run in runs:
                d = os.path.basename(run)
                f = '%s/%s/forces.dat' % (forceDir,d)
                try:
                    data = npy.concatenate( (data, readForceFile(f)), axis = 0 )
                except:                    
                    data = readForceFile(f)

        else:
            forceDir = self.workpath + '/airfoil_%sFoam/postProcessing/forces_%s' % (solver,shape)
            runs = glob.glob(forceDir + '/*')
            for run in runs:
                d = os.path.basename(run)
                f = '%s/%s/forces.dat' % (forceDir,d)
                try:
                    data = npy.concatenate( (data, readForceFile(f)), axis = 0 )
                except:                    
                    data = readForceFile(f)


        return data
            

    def getLift(self,solver,shape):
        iZp = 3
        iZv = 6
        data = self.getForcesFOAM(solver,shape)
        return data[:,[0,iZp,iZv]]

    def getDrag(self,solver,shape):
        iZp = 1
        iZv = 4
        data = self.getForcesFOAM(solver,shape)
        return data[:,[0,iZp,iZv]]

            


    # Given a shape/time, return the pressure vs. coord
    def surfacePressure(self,shape,time,solver='simple'):
        dirname = self.workpath + '/airfoil_%sFoam/VTK/airfoil_%s' % (solver,shape)
        curve = 'surface_%s.curve' % (str(time).zfill(3))
        cfile = '%s/%s' % (dirname,curve)
        dd = loadCurve(cfile)
        if not dd:
            self.visitDriver('Forces',solver,'p',[time],shape=shape)
            dd = loadCurve(cfile)

        x = dd['x'][:,1]
        y = dd['y'][:,1]
        p = dd['p'][:,1]
        nx= dd['nx'][:,1]
        ny= dd['ny'][:,1]
        Fx= dd['Fx'][:,1]
        Fy= dd['Fy'][:,1]
                
        # Order the points
        # Top points are when ny > 0
        # Botton when ny < 0
        top = []
        bot = []
        for i in range(len(nx)):
            if (ny[i]>=0):
                #top.append(i)
                top.append([x[i],p[i]])
            else:
                #bot.append(i)
                bot.append([x[i],p[i]])
                
        top = npy.array(top)
        bot = npy.array(bot)
        itop = npy.argsort(top[:,0])
        ibot = npy.argsort(bot[:,0])

        xx = npy.concatenate( (top[itop,0],bot[ibot[::-1],0]) )
        pp = npy.concatenate( (top[itop,1],bot[ibot[::-1],1]) )
        #idx = distanceOrder(x,10.0*y)
        #return [ x[idx],p[idx] ]
        return [xx,pp]

    # Given a shape/time, return the pressure vs. coord
    def getForces(self,shape,time,solver='simple'):
        dirname = self.workpath + '/airfoil_%sFoam/VTK/airfoil_%s' % (solver,shape)
        curve = 'surface_%s.curve' % (str(time).zfill(3))
        cfile = '%s/%s' % (dirname,curve)
        dd = loadCurve(cfile)
        if not dd:
            print "Run self.visitDriver('Forces',solver,var,times,shape) to make the plot data"
            return None

        Fx= dd['Fx'][:,1]
        Fy= dd['Fy'][:,1]

        return [npy.sum(Fx),npy.sum(Fy)]

    
## Shape class for simple 2d objects
class WTshape():
    
    def __init__(self,stype,name):
        self.type = stype
        self.name = name
        self.args = {}
        self.stl = ''         # STL text string
        self.wake = False


    def setOption(self,options):        
        self.args = self.args.update(options)

    def getSTL(self):

        if (self.type == 'circle'):
            stl = self.circle()
        elif (self.type == 'square'):
            stl = self.square()
        elif (self.type == 'triangle'):
            stl = self.triangle()
        elif (self.type == 'NACA'):
            stl = self.NACA()
        elif (self.type == 'user'):
            stl = self.userSTL()
        elif (self.type == 'poly'):
            if self.args.has_key('sides'):
                nsides = self.args['sides']
            else:
                nsides = 4
                print "Warning: No number of sides specified; using 4"
            stl = self.poly(nsides)
        
        self.stl = stl

    def userSTL(self):
        stl_file = self.args['file']
        lines = self.openSTL(stl_file)
        lines = stllib.centerSTL(lines)

        # Apply args
        if (self.args.has_key('swap')):
            if self.args['swap'] == 'XY':
                lines = stllib.switchXY(lines)
            if self.args['swap'] == 'YZ':
                lines = stllib.switchYZ(lines)

        stl = self.applyArgs(lines)
        return stl
                

    def poly(self,nsides):
        lines = poly_stl(1.0, nsides, 1.0)
        lines = stllib.switchYZ(lines)
        stl = self.applyArgs(lines)
        return stl

    def circle(self):
        lines = poly_stl(1.0, 100, 1.0)
        lines = stllib.switchYZ(lines)
        stl = self.applyArgs(lines)
        return stl

    def square(self):
        lines = poly_stl(1.0, 4, 1.0)
        lines = stllib.switchYZ(lines)
        stl = self.applyArgs(lines)
        return stl

    def triangle(self):
        lines = poly_stl(1.0, 3, 1.0)
        lines = stllib.switchYZ(lines)
        stl = self.applyArgs(lines)
        return stl

    def NACA(self):
        naca_src = SRC_DIR + "/../STL/NACA2STL.m"
        naca_dst = self.workpath + "/NACA2STL.m"

        # Move NACA file to local directory
        shutil.copy(naca_src,naca_dst)

        naca_code = [0,0,1,5]
        if self.args.has_key("Number"):
            naca_code = self.args["Number"]

        # Apply arguments to the Octave script
        stl_file = "naca_tmp.stl"
        dst = {}
        dst["##STLFILE##"] = stl_file
        dst["##NACACODE##"] = str(naca_code).replace(","," ")
        dst["##NACANAME##"] = self.name
        farts(naca_dst,dst)
        
        # Execute octave script
        cmd = "octave %s" % (naca_dst)
        subprocess.call(cmd, shell=True)

        # Read in the STL file and return the lines
        lines = self.openSTL(stl_file)
        stl = self.applyArgs(lines)
        return stl
        
    def openSTL(self,root_file):

        # Get the unit source
        #stl_file = STL_DIR + root_file
        stl_file = root_file
        fid = open(stl_file,'r')
        lines = fid.readlines()
        fid.close()
        return lines

    def applyArgs(self,lines):

        # Apply arguments
        stl_lines = copy.copy(lines)        

        ## Scale in X,Y,Z
        if ( self.args.has_key('scale') ):
            scale = self.args['scale']
            stl_lines = stllib.stl_scale(stl_lines,
                                         scale[0],
                                         scale[1],
                                         scale[2])
        ## Rotate (angle of attack)
        if ( self.args.has_key('alpha') ):
            alpha = self.args['alpha']
            stl_lines = stllib.stl_rotate(stl_lines,
                                          alpha)
        ## Translate object
        if ( self.args.has_key('center') ):
            center = self.args['center']
            stl_lines = stllib.stl_move(stl_lines,
                                        center[0],
                                        center[1],
                                        center[2])

        # Apply name
        stl_lines = stllib.setName(stl_lines,self.name)

        # Make into single string
        stl = "".join(stl_lines)
        return stl
    

class refinementBox():

    def __init__(self):
        self.name = 'refBox'
        self.min = [-1, -1, -1] 
        self.max = [5 ,  1,  1]
        self.levels = [1e15, 2]


def RefBoxSet(boxes):
    out = ''
    #if boxes:
    pre = """refinementRegions
    { \n """

    mid = ""
    for box in boxes:
        mid += """
          %s
          {
             mode inside;
             levels ((%s %s));
           } \n """ % ( box.name,
                        box.levels[0],
                        box.levels[1] )
    post = " } \n "
    out = pre + mid + post
    return out
        
def RefBoxDef(boxes):
    mid = ''
    for box in boxes:
        mid += """ %s
                  {
                    type searchableBox;
                    min ( %s %s %s);
                    max ( %s %s %s);
                  }\n\n
              """ % (box.name,
                     box.min[0], box.min[1], box.min[2],
                     box.max[0], box.max[1], box.max[2])
    return mid



####################################################
## loadCurve(curveFile,curveName)
## Desc: Helper function to load curve file and
##       extract the xy data as a numpy array
####################################################
def loadCurve(cfile):
    try:
        fid = open(cfile,'r')
        lines = fid.readlines()
    except:
        print "Error: Cant open file: %s" % (cfile)
        return None   
    data = {}
    #import pdb;pdb.set_trace()
    for line in lines:
        if ( r'# ' in line ):
            key = line.replace(r'#','').strip()
            data[key] = []
        else:
            ss = line.split(' ')
            x = float(ss[0])
            y = float(ss[1])
            data[key].append([x,y])
    for key in data:
        data[key] = npy.array( data[key] )
    return data

# Helper function to order points around non-circular objects
def distanceOrder(x,y):
    # Get the distance matrix
    ipts = len(x)
    dst = npy.zeros((ipts,ipts))
    for i in range(ipts):
        for j in range(i):
            xd = x[j]-x[i]
            yd = y[j]-y[i]
            d = npy.sqrt( xd**2 + yd**2 )
            dst[i,j] = d
            dst[j,i] = d
    big = npy.max( dst ) * 2.0            
    # Algorithm
    index = []   # index list for order
    iold = npy.argmin(x)  # starting index (lowest x value)    
    index.append(iold)
    dst[iold,iold] = big
    
    while len(index) < ipts:        
        inxt = npy.argmin( dst[iold,:] )
        dst[iold,:] = big
        dst[:,iold] = big
        dst[inxt,inxt] = big
        iold = inxt
        index.append( iold )
    return index

# Make helper function...    
def readForceFile(ffile):
    fid = open(ffile,'r')
    f_lines = fid.readlines()
    data = []
    for fline in f_lines:
        # Skip commented lines
        if fline.strip()[0] == '#':
            pass
        else:
            fil = fline.strip().replace('(','').replace(')','').replace('\t',' ').replace('\n','')
            dat = filter(None,fil.split(' '))
            data.append(dat)
    return npy.array(data).astype(npy.float)

class WindTunnel(foiled):
    pass
