import os
import sys
import glob

def farts(filename,fartdict):
    for frt in fartdict:
        fart(filename,frt,fartdict[frt])
        


def fart(filename,repStr,withStr,debug=False,verbose=False):
    f1 = open(filename, 'r')
    src = f1.readlines()
    f1.close()
    withStr = str(withStr)
    if (not debug):
        f2 = open(filename, 'w')
    for line in src:
        #f2.write(line.replace(repStr,withStr))
        if (repStr in line):
            if verbose:
                print line.replace(repStr,'%s(--%s--)' % (withStr,repStr))
            if (not debug):
                line = line.replace(repStr,withStr)
        if (not debug):
            f2.write(line)
    if (not debug):
        f2.close()

