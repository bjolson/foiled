# The Foiler Quickstart #

### Foiler is a simple python library to setup, manage, run and analyze CFD simulations using OpenFOAM.  It's geared for simplicity and speed but easily extensible to higher fidelity models and increased complexity. 

A quick start quide to using the Foiler python interface for running OpenFOAM as a virtual windtunnel.

***
## Pre-requesites
* [OpenFOAM](http://www.openfoam.org/download/) (installed and properly included in path; eg. typing 'simpleFoam -help' should work)
* Python
    * numpy, matplotlib
* [Octave](https://www.gnu.org/software/octave/) (if you want to use our airfoil generator)
* [Visit](https://wci.llnl.gov) (Open source CFD flow visualizer from LLNL) 

***
## Installation
* Clone the foiler git repository anywhere
```
#!bash
git clone https://bjolson@bitbucket.org/bjolson/foiled.git
```
* Add the source to your python path.
```
#!bash
export PYTHONPATH=$PYTHONPATH:/path/to/foiler/src
```

***
## Tutorials
### Flow past a cylinder
This is a minimal working example to make sure that all the pieces are in the right place.  This should take less that 5 minutes to setup and run and at the end you'll have some nice laminar-ish flow over a cylinder. (see below)

![u_zoom.png](https://bitbucket.org/repo/5eGeeR/images/1875808419-u_zoom.png)

#### Step 1 - Starting the foiler environment
* Launch python
```
#!bash
python
```
* Import the library
```
#!python
from foiler import *
```

#### Step 2 - Creating the flow domain: Your virtual windtunnel!
* Initialize a windtunnel object by giving it a name; "cylinder" in our case.  Set up the work path.
```
#!python
tunnel = WindTunnel("cylinder")
```
* Set the free stream velocity in m/s
```
#!python
tunnel.Uinf = 0.1  # free-stream x-velocity (m/s)
```


#### Step 3 - Add a shape to the tunnel
* The 'shape' class is initialized with a type ('circle') and a name ('myCircle').
```
#!python
cyl = WTshape("circle","myCircle")
```
* Set arguments for (x,y,z) scaling and centering.  There are many others, more later.
```
#!python
cyl.args['scale'] = [1,1,1]
cyl.args['center'] = [0,-.5,0.0]
```
* Add the shape to the tunnel
```
#!python
tunnel.addShape(cyl)
```

#### Step 4 - Make the computational mesh
* We use "snappyHexmesh" to convert .stl files and your settings into a mesh.  This will take about 10 seconds for this case.
```
#!python
tunnel.makeHexMesh()
```
* We can also plot the mesh to check all is well.  A window will pop up; you should see something like this.
```
#!python
tunnel.showMesh()
```
![mesh.png](https://bitbucket.org/repo/5eGeeR/images/4009391320-mesh.png)


#### Step 5 - Run OpenFOAM solver, simpleFoam
* This will run simpleFoam for 100 time steps.
```
#!python
tunnel.runSimple(100)
```

### Step 6 - Post-process the data
* Make plots of the velocity magnitude.
```
#!python
tunnel.visitDriver('Field','simple','umag',[1,5,10])
```
* Display the image of the 10th visualization file
```
#!python
tunnel.showField(10)
```
![cyl_U.png](https://bitbucket.org/repo/5eGeeR/images/3695114707-cyl_U.png)


### Step 7 - Compute the Lift and Drag
* Quantitative analysis is useful for design and optimization.  Getting the forces on a certain object at a certain time is done as follows:
```
#!python
tunnel.visitDriver('Forces','simple','p',[10],shape='myCircle')
[D,L] = tunnel.getForces('myCircle',10)
print "Lift: %s" % L
print "Drag: %s" % D
```
Output:
```
#!bin
Lift: -6.4505734e-05
Drag: 0.001085844202
```
### Step 8 - Get native L & D data
```
#!python
LT = tunnel.getLift('simple','myCircle')
DT = tunnel.getDrag('simple','myCircle')

```

### Step 9 - Plot L & D over time
```
#!python
import matplotlib.pyplot as plt
plt.plot( LT[:,0], LT[:,1] )
plt.hold('on')
plt.plot( DT[:,0], DT[:,1] )
plt.show()
```

That's it for the cylinder case.  A python script for this example is in [foiled/examples/cylinder.py](https://bitbucket.org/bjolson/foiled/src/6919a2ff1ecae44eca56712024c5ad7c39c72a8f/examples/cylinder.py) which you can run all at once by typing:
```
#!bin
python cylinder.py
```

### Next Tutorial: [Flow past an NACA airfoil](https://bitbucket.org/bjolson/foiled/wiki/NACA%20airfoil%20with%20angle%20of%20attack%20)